import React from 'react';
import styled from 'styled-components';
import {$typography} from "./theme";

const FooterComponent = styled.footer`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  grid-area: footer;
  color: ${$typography.lite};
  line-height: 22px;
  margin-top: -32px;
`;

export const Footer = ({}) => {
  return (
    <FooterComponent>
      Binance Smart Chain instant game, { (new Date).getFullYear() }
    </FooterComponent>
  );
};
