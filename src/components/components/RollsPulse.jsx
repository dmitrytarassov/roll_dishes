import React from 'react';
import {Sidebar, NoRolls} from "./";

export const RollsPulse = () => {
  const [bets, setBets] = React.useState(0);

  return (
    <Sidebar
      title="Rolls pulse"
      info={`${bets} rolls`}
      sidebarId={2}
    >
      { bets === 0 ? <NoRolls /> : 'asd' }
    </Sidebar>
  );
};
