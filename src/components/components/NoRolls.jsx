import React from 'react';
import styled from 'styled-components';
import {$typography} from "./theme";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%;
`;

const Message = styled.span`
  color: ${$typography.dark};
  font-size: 26px;
  line-height: 40px;
  font-weight: 300;
`;

export const NoRolls = () => {
  return (
    <Container>
      <Message>No rolls... yet</Message>
    </Container>
  );
};
