const RollDishes = artifacts.require('./RollDishes.sol')

require('chai')
  .use(require('chai-as-promised'))
  .should()

contract('RollDishes', ([deployer, seller, buyer]) => {
  let RollDishes;
  let gamesCount;

  before(async () => {
    RollDishes = await RollDishes.deployed();
    gamesCount = await RollDishes.gamesCount();
  })

  describe("game", async () => {
    let games;
    let newGamesCount = gamesCount;
    before(async () => {
      games = await RollDishes.games(seller);
    });

    it("check games", async () => {
      assert.equal(games.rollNumber.toString(), "0");
    });

    describe("error game", async () => {
      it("check game will be throw without (or with wrong) bet", async () => {
        await RollDishes.game({
          from: seller
        }).should.be.rejected;
        await RollDishes.game({
          from: seller,
          value: web3.utils.toWei('0.05', 'Ether')
        }).should.be.rejected;
        await RollDishes.game({
          from: seller,
          value: web3.utils.toWei('6', 'Ether')
        }).should.be.rejected;
      })
    });

    describe("first game", async function () {
      before(async () => {
        await RollDishes.game({
          from: seller,
          value: web3.utils.toWei('0.1', 'Ether')
        });
        games = await RollDishes.games(seller);
        newGamesCount = await RollDishes.gamesCount();
      });

      it("check games count", async () => {
        console.log("rollNumber", games.rollNumber.toString());
        console.log("prize", games.prize.toString());
        console.log("dish1LastRoll", games.dish1LastRoll.toString());
        console.log("dish2LastRoll", games.dish2LastRoll.toString());
        console.log("lastRollResult", games.lastRollResult.toString());

        assert.equal(newGamesCount.toString(), "1");
        assert.equal(games.rollNumber.toString(), games.lastRollResult.toString() === "win" ? "1" : "0");
        assert.oneOf(games.lastRollResult.toString(), ["win", "loose"]);
      });
    });

    describe("win game", async function () {
      let games;
      before(async () => {
        let win = false;

        while (!win) {
          await RollDishes.game({
            from: seller,
            value: web3.utils.toWei('0.1', 'Ether')
          });
          games = await RollDishes.games(seller);
          if (games.lastRollResult.toString() === "win") {
            win = true;
          }
        }
      });

      it("check win game", async () => {
        console.log(games.rollNumber.toString());
        console.log(games.dish1LastRoll.toString());
        console.log(games.dish2LastRoll.toString());
        console.log(games.prize.toString());
        assert.equal(games.prize.toString(), `${web3.utils.toWei('0.1', 'Ether') * +games.dish2LastRoll.toString()}`);
      });

    });
  });
})
