import React from 'react';
import styled from 'styled-components';
import {Button} from "./";
import {$blackMain, $gray, $titleBackground} from "./theme";

const MenuIcon = () => (
  <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M10 2C10 3.10457 9.10457 4 8 4C6.89543 4 6 3.10457 6 2C6 0.895431 6.89543 0 8 0C9.10457 0 10 0.895431 10 2ZM10 8C10 9.10457 9.10457 10 8 10C6.89543 10 6 9.10457 6 8C6 6.89543 6.89543 6 8 6C9.10457 6 10 6.89543 10 8ZM8 16C9.10457 16 10 15.1046 10 14C10 12.8954 9.10457 12 8 12C6.89543 12 6 12.8954 6 14C6 15.1046 6.89543 16 8 16Z" fill="white"/>
  </svg>
);

const HeaderContainer = styled.div`
  width: 100%;
  display: flex;
  height: 44px;
  grid-area: header;
  justify-content: flex-end;
`;

const AccountInfo = styled.div`
  background: ${$blackMain};
  border-radius: 4px;
  font-size: 16px;
  line-height: 20px;
  padding: 12px 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
`;

const MenuContainer = styled.div`
  margin-left: 8px;
  cursor: pointer;
`;

const Menu = ({ toggleMenuIsOpen, menuIsOpen }) => {
  const ref = React.useRef();

  React.useEffect(() => {
    const listener = (e) => {
      let { target } = e;
      while (target && !document.body.isSameNode(target)) {
        target = target.parentNode;
        if (target && ref.current && ref.current.isSameNode(target)) {
          return ;
        }
      }
      toggleMenuIsOpen(false);
    };

    window.addEventListener('click', listener);

    return () => {
      window.removeEventListener('click', listener);
    }
  }, []);

  const onClick = () => {
    toggleMenuIsOpen(!menuIsOpen);
  }

  return (
    <MenuContainer ref={ref} onClick={onClick}>
      <MenuIcon />
    </MenuContainer>
  )
}

const SubMenu = styled.div`
  position: absolute;
  width: 300px;
  padding: 24px;
  background: ${$titleBackground};
  top: 110%;
  right: 0;
`;

const getShortHash = (string) => {
  const arr = string.split('');

  return `${arr.splice(0, 5).join('')}...${arr.reverse().splice(0, 5).join('')}`
}

export const Header = ({ connected, connect, disconnect, loading, account }) => {
  const [menuIsOpen, toggleMenuIsOpen] = React.useState(false);

  React.useEffect(() => {
    if (!connected) {
      toggleMenuIsOpen(false);
    }
  }, [connected]);

  return (
    <HeaderContainer>
      {
        account && connected ? <AccountInfo>
          { getShortHash(account) } <Menu toggleMenuIsOpen={toggleMenuIsOpen} menuIsOpen={menuIsOpen} />
          { menuIsOpen && <SubMenu>
            <Button fullwidth onClick={disconnect}>Disconnect</Button>
          </SubMenu>}
        </AccountInfo> : <Button onClick={connect}>
          Connect
        </Button>
      }
    </HeaderContainer>
  );
};
