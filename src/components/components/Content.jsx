import React from 'react';
import styled from 'styled-components';
import {$containerBackground} from "./theme";
import {TrapezoidButton} from "./TrapezoidButton";

const ContentContainer = styled.div`
  width: 100%;
  display: flex;
  min-height: 100%;
  grid-area: content;
  padding-bottom: 32px;
`;

const Box = styled.div`
  background: ${$containerBackground};
  border-radius: 8px;
  width: 100%;
  display: flex;
  min-height: 100%;
  flex-direction: column;
  justify-content: space-between;
  padding: 32px;
`;

const ButtonContainer = styled.div`
  width: 100%;
  padding: 0 16px;
`;

export const Content = ({ connect, connected, roll, loading, gameIsLoaded, game }) => {
  const onClick = (connected && gameIsLoaded && game) ? roll : connect;
  const buttonText = (connected && gameIsLoaded) ? game ? "Roll the dice" : '' : "Connect wallet";
  const style = (connected && gameIsLoaded) ? "active" : "normal";

  return (
    <ContentContainer>
      <Box>
        <ButtonContainer>
          as
        </ButtonContainer>
        <ButtonContainer>
          <TrapezoidButton onClick={onClick} style={style} loading={loading}>{ buttonText }</TrapezoidButton>
        </ButtonContainer>
      </Box>
    </ContentContainer>
  );
};
