import React from 'react';
import styled from 'styled-components';
import {$yellowGradient, $yellowMain, $gray, $typography} from "./theme";

const ButtonContainer = styled.button`
  &, &:focus {
    background: ${$yellowGradient};
    border: 1px solid ${$yellowMain};
    border-radius: 4px;
    font-size: 16px;
    line-height: 20px;
    padding: 12px 24px;
    -webkit-tap-highlight-color: transparent;
    outline: none;
  }
  
  &.fullwidth {
    width: 100%;
  }
  
  &:hover {
    outline: none;
    border: 1px solid ${$yellowMain};
    background: transparent;
    color: #fff;
  }
  
  &:disabled {
    background: ${$gray};
    border-color: ${$gray};
    cursor: auto;
    color: ${$typography.dark};
    
    &:hover {
      background: ${$gray};
      border-color: ${$gray};
      color: ${$typography.dark};
    }
  }
`;

export const Button = ({ onClick = () => {}, children, loading, disabled, fullwidth = false }) => {
  const className = [];
  if (fullwidth) {
    className.push("fullwidth");
  }

  return (
    <ButtonContainer onClick={onClick} disabled={disabled} className={className}>
      { loading ? "..." : children }
    </ButtonContainer>
  );
};
