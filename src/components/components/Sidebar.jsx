import React from 'react';
import styled from 'styled-components';
import {$containerBackground, $gray, $titleBackground} from "./theme";

const SidebarContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  height: 100%;
  background: ${$containerBackground};
  border-radius: 8px;
  overflow: hidden;
`;

const SidebarTitle = styled.div`
  width: 100%;
  display: flex;
  height: 70px;
  justify-content: space-between;
  font-weight: 700;
  background-color: ${$titleBackground};
  line-height: 22px;
  padding: 24px;
  font-size: 16px;
  text-transform: uppercase;
`;

const SidebarTitleInfo = styled.span`
  color: ${$gray};
`;

const SidebarContent = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100%;
`;

export const Sidebar = ({ title, info, children, sidebarId }) => (
  <SidebarContainer style={{ gridArea: `sidebar${sidebarId}` }}>
    <SidebarTitle>
      <span>{ title } </span>
      <SidebarTitleInfo>{ info }</SidebarTitleInfo>
    </SidebarTitle>
    <SidebarContent>{ children }</SidebarContent>
  </SidebarContainer>
);
