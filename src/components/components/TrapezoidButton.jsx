import React from 'react';
import styled from 'styled-components';
import {
  $buttonLoadingBackground, $buttonLoadingBorder,
  $gray,
  $pinkGradient,
  $pinkMain,
  $typography,
  $yellowGradient,
  $yellowMain
} from "./theme";

const ButtonText = styled.span`
  position:relative;
  z-index: 2;
  font-weight: 400;
  font-size: 26px;
  line-height: 36px;
  color: #000;
  
  .pink &, .loading & {
    color: #fff;
  }
`;

const Trapezoid = styled.div`
  background: ${$yellowGradient};
  border: 2px solid ${$yellowMain};
  border-radius: 4px;
  font-size: 16px;
  line-height: 20px;
  padding: 12px 24px;
  -webkit-tap-highlight-color: transparent;
  outline: none;
  transform: perspective(32px) rotateX(2deg);
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1;
  box-shadow: 0px 4px 44px rgba(0, 0, 0, 0.35);
  
  .pink & {
    background: ${$pinkGradient};
    border-color: ${$pinkMain};
  }
  
  .loading & {
    background: ${$buttonLoadingBackground};
    border-color: ${$buttonLoadingBorder};
  }
`;

const TrapezoidButtonContainer = styled.div`
  position: relative;
  width: 100%;
  height: 88px;
  display:flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  
  &.loading {
    cursor: default;
  }
  
  &:not(.loading):hover {
    ${Trapezoid} {
      background: transparent;
    }
    
    ${ButtonText} {
      color: #fff;
    }
  }
`;

export const TrapezoidButton = ({ onClick = () => {}, children, loading, disabled, style }) => {
  let className = '';
  if (style === 'active') {
    className = 'pink';
  }
  if (loading) {
    className = 'loading';
  }

  return (
    <TrapezoidButtonContainer onClick={onClick} disabled={disabled} className={className}>
      <Trapezoid />
      <ButtonText>{ children }</ButtonText>
    </TrapezoidButtonContainer>
  );
};