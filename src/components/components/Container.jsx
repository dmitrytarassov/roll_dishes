import React from 'react';
import styled from 'styled-components';

const areas = `
'header header header'
'sidebar1 content sidebar2'
'footer footer footer'
`;

const MainContainer = styled.div`
  max-width: 1328px;
  width: 100vw;
  min-height: 100vh;
  display: grid;
  grid-template-columns: 300px 1fr 300px;
  grid-template-rows: 44px 1fr;
  grid-row-gap: 32px;
  grid-column-gap: 40px;
  padding: 32px 0;
  grid-template-areas: ${areas};
`;

export const Container = ({ children }) => {
  return (
    <MainContainer>
      { children }
    </MainContainer>
  );
};