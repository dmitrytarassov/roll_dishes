require('babel-register');
require('babel-polyfill');

// 0xb26B3e7D10B146061CE1995D753340eFd60aC5Bd
const HDWalletProvider = require('@truffle/hdwallet-provider');
const provider = new HDWalletProvider({
  privateKeys: ['d2ec37f6232f0c640603f59a0e9c68f3b3f73ad1633a9c37e1c53399c77f43d0'],
  providerOrUrl: 'https://data-seed-prebsc-1-s1.binance.org:8545/'
});

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "5777"
    },
    binanceTestnet: {
      provider: () => provider,
      network_id: "97",
      gas: 1000000
    },
  },
  contracts_directory: './src/contracts/',
  contracts_build_directory: './src/abis/',
  compilers: {
    solc: {
      version: "0.7.4",
      // optimizer: {
      //   enabled: true,
      //   runs: 200
      // }
    }
  }
}
