import React from 'react';
import {Sidebar, NoRolls} from "./";

export const MyBets = () => {
  const [bets, setBets] = React.useState(0);

  return (
    <Sidebar
      title="My bets"
      info={bets}
      sidebarId={1}
    >
      { bets === 0 ? <NoRolls /> : 'asd' }
    </Sidebar>
  );
};
