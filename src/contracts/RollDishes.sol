pragma solidity ^0.7.4;

contract RollDishes {
    uint private nonce = 123;
    uint public gamesCount = 0;
    mapping(address => Game) public games;
  
    struct Game {
        uint rollNumber;
        address payable gamer;
        uint dish1LastRoll;
        uint dish2LastRoll;
        string lastRollResult;
        uint prize;
    }
    
    event RollComplete();
    
    function random() private returns (uint) {
        uint randomNumber = uint(keccak256(abi.encodePacked(block.timestamp, msg.sender, nonce))) % 6;
        randomNumber = randomNumber + 1;
        nonce++;
        return randomNumber;
    }
    
    
    function game() public payable {
        // Check the bet
        require(msg.value >= 0.1 ether && msg.value <= 5 ether);
        
        // init game
        if (games[msg.sender].rollNumber < 0) {
            games[msg.sender] = Game(0, msg.sender, 0, 0, "no games", 0);
        }
        // find game
        Game memory _game = games[msg.sender];
    
        // roll dish 1
        _game.dish1LastRoll = random();
        // roll dish 2
        _game.dish2LastRoll = random();
        
        // increment games count
        gamesCount ++;
        
        if (_game.dish1LastRoll == _game.dish2LastRoll) {
            // increment gamer rolls number
            _game.rollNumber ++;
            // set gamer last roll result
            _game.lastRollResult = "win";
            // set gamers prize
            _game.prize = _game.prize + _game.dish1LastRoll * msg.value;
            // send money to gamer
            msg.sender.transfer(_game.prize);
        } else {
            // give the bet
            address(this).call{value:msg.value}("");
            // set gamers prize to 0
            _game.prize = 0;
            // set rolls number to 0
            _game.rollNumber = 0;
            // set gamer last roll result
            _game.lastRollResult = "loose";
        }
        
        // save game
        games[msg.sender] = _game;
        // emit he event
        // does we need that?
        emit RollComplete();
    }
}
