export const $gray = '#AFAFAF';
export const $titleBackground = '#2D2D2D';
export const $containerBackground = '#202020';
export const $typography = {
  dark: '#737373',
  black: '#181818',
  lite: '#CDCDCD',
};
export const $buttonLoadingBackground = '#2A2A2A';
export const $buttonLoadingBorder = '#161616';
export const $blackMain = '#272727';
export const $yellowMain = '#FFC62D';
export const $yellowSecondary = '#EBB019';
export const $yellowGradient = `linear-gradient(95.23deg, ${$yellowMain} 10.33%, ${$yellowSecondary} 90.78%)`;
export const $pinkMain = '#A82AD5';
export const $pinkSecondary = '#9416C0';
export const $pinkGradient = `linear-gradient(99.67deg, ${$pinkMain} 10.18%, ${$pinkSecondary} 102.78%);`;
