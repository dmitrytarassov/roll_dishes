import React, { Component } from 'react';
import Contract from 'web3-eth-contract';
import './App.css';
import RollDishes from '../abis/RollDishes.json'
import Web3 from 'web3'
import random from "lodash/random";
import {Container, MyBets, RollsPulse, Header, Content, Footer} from "./components";

let web3;
const CONNECTED_KEY = "CONNECTED_KEY";

async function loadBlockchainData() {
  // Load smart contract
  const networkId = await window.ethereum.networkVersion;
  console.log(networkId);
  console.log(RollDishes.networks);
  const networkData = RollDishes.networks[networkId];
  console.log(networkData);
  if(networkData) {
    const abi = RollDishes.abi;
    const address = networkData.address;
    console.log(abi, address);
    console.log(Contract);
    const game = new Contract(abi, address)
    return game;
  } else {
    return null;
  }
}

const App = () => {
  const [connected, setConnected] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [account, setAccount] = React.useState(null);
  const [rollLoading, setRollLoading] = React.useState(false);
  const [gameIsLoaded, setGameIsLoaded] = React.useState(false);
  const [game, setGame] = React.useState(null);

  const loadGame = async () => {
    const game = await loadBlockchainData();
    setGame(game);
    setGameIsLoaded(true);
  }

  const connect = async () => {
    setLoading(true);
    if (window.ethereum) {
      try {
        const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
        if (accounts.length) {
          setAccount(accounts[0]);
          setConnected(true);
          await loadGame();
          localStorage.setItem(CONNECTED_KEY, "1");
        }
      } catch(e) {
        console.log(e);
        console.log('e');
      }
    }
    else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider);
      setConnected(true);
      await loadGame();
      localStorage.setItem(CONNECTED_KEY, "1");
    }
    else {
      window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }
    setLoading(false);
  }

  const disconnect = () => {
    setConnected(false);
    localStorage.removeItem(CONNECTED_KEY);
  }

  React.useEffect(() => {
    if (localStorage.getItem(CONNECTED_KEY)) {
      connect();
    }
  }, []);

  const roll = () => {
    if (!rollLoading) {
      console.log("roll");
      setRollLoading(true);
      setTimeout(() => {
        setRollLoading(false);
      }, random(1000, 5000));
    }
  }

  return (
    <Container>
      <Header
        connected={connected}
        connect={connect}
        disconnect={disconnect}
        loading={loading}
        account={account}
      />
      <MyBets />
      <Content
        connect={connect}
        connected={connected}
        roll={roll}
        loading={rollLoading}
        game={game}
        gameIsLoaded={gameIsLoaded}
      />
      <RollsPulse />
      <Footer />
    </Container>
  );
}

class Appw extends Component {
  state = {
    connected: false,
    data: null,
    bet: 0.1,
    loading: true,
    balance: 0,
    previous: 0
  }

  constructor() {
    super();
    window.a = this;
  }

  async componentWillMount() {
    // await this.loadWeb3()
    // await this.loadBlockchainData()
    // await this.loadGameStats();
  }

  async loadGameStats() {
    if (this.state.game) {
      const _game = await this.state.game.methods.games(this.state.account);
      if (_game) {
        const data = await _game.call();
        this.setState({ data, loading: false });
      }
      if (this.state.account) {
        let previous = this.state.balance;
        const balance = await window.web3.eth.getBalance(this.state.account);
        if (!this.state.balance) {
          previous = balance;
        }
        this.setState({
          balance,
          previous
        });
      }
    }
  }

  async loadWeb3() {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable()
      this.setState({ connected: true })
    }
    else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    }
    else {
      window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }
  }

  async loadBlockchainData() {
    const web3 = window.web3
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })

    // Load smart contract
    const networkId = await web3.eth.net.getId()
    console.log(networkId);
    console.log(RollDishes.networks);
    const networkData = RollDishes.networks[networkId]
    if(networkData) {
      const abi = RollDishes.abi
      const address = networkData.address
      const game = new web3.eth.Contract(abi, address)
      this.setState({ game });
    } else {
      alert('Smart contract not deployed to detected network.')
    }
  }

  roll = async () => {
    this.setState({ loading: true })
    const { game, account, bet } = this.state;
    if (game) {
      await game.methods.game().send({
        from: account,
        value: window.web3.utils.toWei(`${bet}`, 'Ether'),
      }).once("confirmation", async () => {
        await this.loadGameStats();
      }).once("error", (error) => {
        console.log(error);
        this.setState({ loading: false })
      });

    }
  }

  render() {
    return (
      <Container>
        <Header />
        <MyBets />
        <Content />
        <RollsPulse />
      </Container>
    );
  }

  render2() {
    const roll = this.roll.bind(this);
    const { previous, balance } = this.state;
    const _previous = +(window.web3.utils.fromWei(`${previous}`, 'Ether'));
    const _balance = +(window.web3.utils.fromWei(`${balance}`, 'Ether'));
    const change = _balance - _previous;
    return (
      <center>
        <div>connected: { this.state.connected.toString() }</div>
        {
          this.state.connected && <>
            <div>
              { this.state.data && <>
                <p>previous: { _previous }</p>
                <p>balance: { _balance }</p>
                <p>change: { change }</p>
                <p>rollNumber: {this.state.data.rollNumber.toString()}</p>
                <p>lastRollResult: {this.state.data.lastRollResult.toString()}</p>
                <p>dish1LastRoll: {this.state.data.dish1LastRoll.toString()}</p>
                <p>dish2LastRoll: {this.state.data.dish2LastRoll.toString()}</p>
                <p>prize: {this.state.data.prize.toString()}</p>
              </>}
            </div>
            <div>
              <input type="number" min={0.1} max={5} value={this.state.bet} onChange={(e) => this.setState({
                bet: e.target.value
              })}/>
            </div>
            <div>
              <button onClick={roll} disabled={this.state.loading}>Roll</button>
            </div>
          </>
        }
      </center>
    );
  }
}

export default App;
